package util;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class MusicPlayer implements Runnable {

	public static final File[] FILES = new File("Resources")
			.listFiles(new FileFilter() {
				public boolean accept(File f) {
					return f.getName().endsWith(".wav");
				}
			});
	java.util.concurrent.ExecutorService service = java.util.concurrent.Executors
			.newCachedThreadPool();
	Sound currentMusic = null;
	boolean mute = false;
	int index = -1;

	ArrayList<Sound> dying = new ArrayList<Sound>();
	ReentrantLock lock = new ReentrantLock();

	public MusicPlayer() {
		this((int) (Math.random() * FILES.length));
	}

	public MusicPlayer(int index) {
		this(new Sound(FILES[index % FILES.length]));
		this.index = index;
	}

	public MusicPlayer(Sound startMusic) {
		currentMusic = startMusic;
		startMusic.setFade(1);
		service.execute(startMusic);
		service.execute(this);
	}

	public void setMusicTo(Sound s) {
		s.setVolume(.05, true);
		service.execute(s);
		s.setFade(2);
		s.loop(-1);
		currentMusic.setFade(-5);
		// dying.add(currentMusic);

		this.currentMusic = s;
	}

	public void setMusicTo(int index) {
		this.index = index;
		setMusicTo(new Sound(FILES[index % FILES.length]));
	}

	public void playSoundEffect(String dir) {
		try {
			Clip c = AudioSystem.getClip();
			AudioInputStream au = AudioSystem
					.getAudioInputStream(new File(dir));
			c.open(au);
			c.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (Exception e) {
			}

			lock.lock();
			for (int i = 0; i < dying.size(); i++) {
				if (dying.get(i).getVolume() < .05 && !mute) {
					dying.get(i).end();
					dying.remove(i);
					i--;
				}
			}
			lock.unlock();
		}
	}

	public void mute(boolean b) {
		if (mute == b) {
			return;
		}
		mute = b;
		currentMusic.mute(b);
	}

	public void finish() {
		service.shutdown();
	}

	public static void main(String[] args) {
		MusicPlayer mp = new MusicPlayer();
		int counter = 0;

		while (true) {
			try {
				Thread.sleep(10000);
			} catch (Exception e) {
			}
			mp.setMusicTo(++counter);
		}
	}

	public int getCurrentIndex() {
		return index;
	}
}
