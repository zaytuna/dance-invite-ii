package util;

import java.awt.geom.Point2D;

public class Vector2D {
	public double x, y;

	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double get(int index) {
		return index == 0 ? x : y;
	}

	public Vector2D clone() {
		return new Vector2D(x, y);
	}

	public Vector2D scale(double d) {
		x *= d;
		y *= d;
		return this;
	}

	public double dot(Vector2D other) {
		return x * other.x + y * other.y;
	}

	public double mag() {
		return Math.sqrt(x * x + y * y);
	}

	public double angleBetween(Vector2D other) {
		return Math.acos(dot(other) / (mag() * other.mag()));
	}

	public Vector2D add(Vector2D v) {
		x += v.x;
		y += v.y;
		return this;
	}

	public Vector2D negate() {
		x *= -1;
		y *= -1;
		return this;
	}

	public Vector2D unitize() {
		double mag = mag();
		x /= mag;
		y /= mag;
		return this;
	}

	public Vector2D perp() {
		double old = x;
		x = -y;
		y = old;
		return this;
	}

	public Vector2D proj(Vector2D other) {
		return other.clone().scale(this.dot(other) / other.mag2());
	}

	@Override
	public String toString() {
		return "Vector2D (" + x + "," + y + ")";
	}

	public void set(double d, double e) {
		x = d;
		y = e;
	}

	public Vector2D minus(Vector2D v) {
		x -= v.x;
		y -= v.y;
		return this;
	}

	public double mag2() {
		return x * x + y * y;
	}

	public Vector2D rotate(double rotation) {
		double c = Math.cos(rotation), s = Math.sin(rotation);
		double a = x * c + -y * s, b = x * s + y * c;
		x = a;
		y = b;
		return this;
	}

	public void set(Vector2D s) {
		x = s.x;
		y = s.y;
	}

	public static Vector2D randomUnit() {
		double t = Math.random() * 2 * Math.PI;
		return new Vector2D(Math.cos(t), Math.sin(t));
	}

	public double angle() {
		return Math.atan2(y, x);
	}

	public Point2D point2D() {
		return new Point2D.Double(x, y);
	}
}
