package main;

import java.awt.Color;
import java.awt.Graphics2D;

import util.Vector2D;

public class Weapon {
	Player owner;
	int type = 0, rate = 400; // 0
	double dmg = 20, speed = .13, blastRadius = 1, hps = 0.2;
	long lastFired;

	Color clr = Color.MAGENTA;

	double heat;
	boolean overheat = false;

	public Weapon(Player p) {
		this.owner = p;
	}

	public void fire(Vector2D pos) {
		long t = System.currentTimeMillis();
		if (!overheat
				&& (t - lastFired > rate * Math.pow(0.8, owner.points[0]))) {
			Projectile proj = new Projectile(dmg * (1 + owner.points[2] / 4D),
					speed, blastRadius * (1 + owner.points[3] / 2D), type,
					owner.pos.clone(), pos.clone(), owner.v);
			heat += hps * Math.pow(0.85, owner.points[1]);
			World.WORLD.shots.add(proj);
			lastFired = t;

			if (heat > 1) {
				heat = 1;
				overheat = true;
			}
		}
	}

	public void update() {
		heat = Math.max(0, heat - 0.002);
		if (heat <= 0)
			overheat = false;
	}

	public void draw(Graphics2D g, int counter) {
		g.setColor(owner.weapon.overheat ? (counter % 30 < 15 ? new Color(180,
				0, 0, 180) : new Color(0, 0, 0, 0)) : new Color(180, 0, 180,
				180));
		g.fillRect(0, 10, 43, 15);
		g.setColor(Color.BLACK);
		g.drawRect(0, 10, 43, 15);
	}
}
