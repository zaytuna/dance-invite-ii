package main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import util.Vector2D;

public class Enemy extends MovingObject {
	public static BufferedImage ICON;

	Vector2D[] v = { new Vector2D(0.1, 0) };
	int hp = 50, max = 50;
	double speed = 0.1;

	public Enemy(Vector2D pos) {
		this.pos = pos;
	}

	public void update(Player p) {
		pos.add(v[0]);
		v[0].add(World.WORLD.gravity(pos));
		if (hp < max)
			v[0].add((p.pos.clone().minus(pos).unitize().scale(speed)
					.minus(v[0]).scale(0.1)));

		if (p.pos.clone().minus(pos).mag() < size / 2 + p.size / 2)
			p.hpTo -= 1;
	}

	public void paint(Graphics2D g, int x, int y, int s) {
		g.drawImage(ICON, x - s / 2, y - s / 2, s, s, null);
		g.setColor(new Color(140, 140, 140, 140));
		g.fillRect(x - 40, y - 10, 80, 20);
		g.setColor(new Color(0, 200, 0, 140));
		g.fillRect(x - 40, y - 10, (80 * hp) / max, 20);
	}

	public Vector2D[] getVelocities() {
		return v;
	}
}
