package main;

import java.awt.Color;
import java.awt.Graphics2D;

import util.Methods;
import util.Vector2D;

public class Spawn {
	Vector2D pos;
	long lastTime, t;
	int time = 2000;
	int hp = 1000, max = 1000;

	public Spawn(Vector2D p) {
		pos = p;
	}

	public void update(Player plr) {
		t = System.currentTimeMillis();
		if (plr.pos.clone().minus(pos).mag2() <= 100 && (t - lastTime) > time) {
			Enemy e = new Enemy(pos.clone());
			e.v[0] = Vector2D.randomUnit().scale(0.05);
			World.WORLD.enemies.add(e);
			lastTime = t;
		}
	}

	public void draw(Graphics2D g, int x, int y, int s) {
		Color c = Methods.colorMeld(Color.GREEN, Color.RED,
				Math.min(Math.max((t - lastTime) / (double) time, 0), 1));
		g.setColor(c);
		g.fillOval(x - s / 2, y - s / 2, s, s);
		g.setColor(c.darker().darker());
		g.fillOval(x - (s * hp) / max / 2, y - (s * hp) / max / 2, (s * hp)
				/ max, (s * hp) / max);
	}
}
