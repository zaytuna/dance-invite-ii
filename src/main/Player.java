package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.util.HashSet;

import util.Vector2D;

public class Player extends MovingObject {
	public static final int UP = 0, LEFT = 1, DOWN = 2, RIGHT = 3, SPECIAL = 4,
			SWITCH = 5;
	public static final String[] ATTRIBUTES = { "Fire Rate", "Overheat", "Damage",
			"Blast" };
	Color color = Color.GREEN;
	int[] keyBindings = { KeyEvent.VK_W, KeyEvent.VK_A, KeyEvent.VK_S,
			KeyEvent.VK_D, KeyEvent.VK_SPACE, KeyEvent.VK_Q };
	double counter = 0, jump;
	double walkDir = 0, lookDir;
	double prc, speed;

	Vector2D grav = new Vector2D(0, 0);

	int weaponNum = -1;

	double hp = 100, max = 100, hpTo = 100, xp = 0;
	int level = 0;

	Vector2D walking = new Vector2D(0, 0), nonwalking = new Vector2D(0, 0),
			v = new Vector2D(0, 0);
	Vector2D[] velocities = { walking, nonwalking, v };

	int[] points = { 0, 0, 0, 0 };// jump, fireRate, dmg, splash
	int unspent = 0;

	public Weapon weapon;

	public Player(Checkpoint c) {
		pos = new Vector2D(c.pos.x, c.pos.y);
		c.active = true;
		jump = 0.2;
		speed = 0.07;
		size = 1;
		unspent = 1;
		weapon = new Weapon(this);
	}

	public void paint(Graphics2D g, ViewPane q) {
		int five = 3;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(new BasicStroke(4));
		g.translate(q.rnd(pos.x - q.view.x), q.rnd(pos.y - q.view.y));
		// g.translate(-180,-180);

		if (grav.mag2() == 0) {
			// draw feet
			if (v.x != 0 || v.y != 0)
				walkDir = Math.atan2(v.y, v.x);
			g.rotate(walkDir);
			g.setColor(color);
			g.fillOval((int) (five * 4 * Math.sin(counter * 2 * Math.PI))
					- five * 3, five, five * 11, five * 8);
			g.fillOval(
					(int) (five * 4 * Math.sin(Math.PI + counter * 2 * Math.PI))
							- five * 3, -five * 9, five * 10, five * 8);
			g.setColor(Color.BLACK);
			g.drawOval((int) (five * 4 * Math.sin(counter * 2 * Math.PI))
					- five * 3, five, five * 11, five * 8);
			g.drawOval((int) (20 * Math.sin(Math.PI + counter * 2 * Math.PI))
					- five * 3, -five * 9, five * 10, five * 8);
			g.rotate(-walkDir);
		}

		if (weaponNum >= 0) {
			g.rotate(lookDir);
			weapon.draw(g, q.counter);
			g.rotate(-lookDir);
		}

		g.setColor(new Color(0, 0, 0, 200));
		g.fillOval(-five * 8, -five * 8, five * 16, five * 16);
		g.setColor(Color.WHITE);
		g.setStroke(new BasicStroke(3));
		g.drawOval(-five * 8, -five * 8, five * 16, five * 16);
		g.translate(-q.rnd(pos.x - q.view.x), -q.rnd(pos.y - q.view.y));

	}

	public void update(HashSet<Integer> keyDown, double plrA) {

		nonwalking.scale(0.98);

		if (xp >= 1) {
			xp -= 1;
			unspent++;
			level++;
		}

		grav = World.WORLD.gravity(pos);
		for (Fence f : World.WORLD.fences)
			if (f.contains(pos))
				grav = new Vector2D(0, 0);

		if (weaponNum >= 0)
			weapon.update();

		hp += (hpTo - hp) / 20;
		hpTo = Math.min(max, hpTo + 0.01);

		nonwalking.add(grav);

		Vector2D m = new Vector2D(0, 0);
		if (keyDown.contains(keyBindings[LEFT])) {
			m.x -= Math.cos(plrA);
			m.y -= Math.sin(plrA);
		}
		if (keyDown.contains(keyBindings[RIGHT])) {
			m.x += Math.cos(plrA);
			m.y += Math.sin(plrA);
		}
		if (keyDown.contains(keyBindings[UP])) {
			m.x += Math.sin(plrA);
			m.y -= Math.cos(plrA);
		}
		if (keyDown.contains(keyBindings[DOWN])) {
			m.x -= Math.sin(plrA);
			m.y += Math.cos(plrA);
		}
		walking.scale(0.9);

		if (m.x == 0 && m.y == 0)
			;
		else
			m.unitize();

		m.scale(0.008);
		walking.add(m);

		if (keyDown.contains(keyBindings[SPECIAL])) {
			nonwalking.add(jumpDir.clone().scale(jump));
		}

		if (walking.mag() > speed)
			walking.unitize().scale(speed);

		double q = v.mag() / 2;
		counter += q + (q < .01 ? ((counter % 1) - 0.5) / -5 : 0);
		counter = counter % 1;

		jumpDir.set(0, 0);

		v.set(walking.x + nonwalking.x, walking.y + nonwalking.y);
		pos.add(v);
	}

	@Override
	public Vector2D[] getVelocities() {
		return velocities;
	}
}
