package main;

import util.Vector2D;

public abstract class MovingObject {
	public Vector2D pos = new Vector2D(0, 0), 
			jumpDir = new Vector2D(0, 0);
	public double size = 1;
	
	public abstract Vector2D[] getVelocities();
}
