package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import util.Methods;
import util.Vector2D;

public class Projectile extends MovingObject {
	public int nparticles = 500, time = 1000;
	int phase = 0;// traveling, exploding, cleanup
	double dmg, speed, blast, type;
	Vector2D start, end, dir;
	Color color = Color.MAGENTA;

	long starttime;

	Vector2D[] explo;
	Vector2D[] velo;

	public Projectile(double d, double s, double b, int t, Vector2D p1,
			Vector2D p2, Vector2D v) {
		this.size = 0.25;
		this.dmg = d;
		this.speed = s;
		this.blast = b;
		this.type = t;
		this.start = p1;
		this.end = p2;
		this.pos = start.clone();
		this.dir = (end.clone().minus(start)).add(v);
		// dir.scale(0.1);
		dir.unitize().scale(speed);
		starttime = System.currentTimeMillis();
	}

	public void update() {
		if (phase == 0) {
			if (/*
				 * Math.abs(pos.x - end.x) < .01 && Math.abs(pos.y - end.y) <
				 * .01 ||
				 */(System.currentTimeMillis() - starttime > time)) {
				explode();

			}
			if (type == 0) {
				dir.add(World.WORLD.gravity(pos));
				pos.add(dir);
				// /if you hit an enemy....
				for (Enemy e : World.WORLD.enemies) {
					if (e.pos.clone().minus(pos).mag2() < .2)
						explode();
				}
				for (Spawn s : World.WORLD.spawners) {
					if (s.pos.clone().minus(pos).mag2() < 1)
						explode();
				}
				// or a mine:
				if (World.WORLD.game.bnds.contains(pos.point2D())) {
					Vector2D v = World.WORLD.game.getSquare(pos);
					if (World.WORLD.game.covered[(int) v.x][(int) v.y] == 1) {
						World.WORLD.game.press((int) v.x, (int) v.y, false);
						explode();
					}
				}
			}
		} else if (phase == 1) {
			if (color.getAlpha() < 20)
				phase = 2;
			color = Methods.colorMeld(color, new Color(0, 0, 0, 0), 0.01);

			for (int i = 0; i < explo.length; i++) {
				explo[i].add(velo[i]);
				velo[i].scale(0.95);
			}
		}

	}

	private void explode() {
		phase = 1;
		explo = new Vector2D[nparticles];
		velo = new Vector2D[nparticles];
		for (int i = 0; i < nparticles; i++) {
			explo[i] = pos.clone();
			double t = Math.random() * 2 * Math.PI;
			velo[i] = new Vector2D(Math.cos(t), Math.sin(t)).scale(Math
					.random() * blast / 50);
		}

		for (Enemy e : World.WORLD.enemies) {
			if (e.pos.clone().minus(pos).mag2() < blast * blast)
				e.hp -= dmg;
		}
		for (Spawn s : World.WORLD.spawners) {
			if (s.pos.clone().minus(pos).mag2() < (blast + .5) * (blast + .5))
				s.hp -= dmg;
		}
	}

	public void paint(Graphics2D g, ViewPane v) {
		if (phase == 0) {
			g.setStroke(new BasicStroke(v.rnd(0.05)));
			g.setColor(color);
			g.fillOval(v.rnd(pos.x - v.view.x - 0.1),
					v.rnd(pos.y - v.view.y - 0.1), v.rnd(0.2), v.rnd(0.2));
			g.setColor(Methods.colorMeld(color, Color.WHITE, 0.5));
			g.drawOval(v.rnd(pos.x - v.view.x - 0.05),
					v.rnd(pos.y - v.view.y - 0.05), v.rnd(0.1), v.rnd(0.1));
			g.setColor(Methods.colorMeld(color, Color.BLACK, 0.5));
			g.drawOval(v.rnd(pos.x - v.view.x - 0.1),
					v.rnd(pos.y - v.view.y - 0.1), v.rnd(0.2), v.rnd(0.2));
		} else if (phase == 1) {
			for (int i = 0; i < explo.length; i++) {
				g.setColor(Methods.colorMeld(color,
						i < explo.length / 2 ? Color.BLACK : Color.WHITE,
						Math.abs(i - explo.length / 2)
								/ (double) (explo.length / 2), color.getAlpha()));
				g.fillRect(v.rnd(explo[i].x - v.view.x),
						v.rnd(explo[i].y - v.view.y), 5, 5);
			}

		}
	}

	@Override
	public Vector2D[] getVelocities() {
		return new Vector2D[] { dir };
	}
}
