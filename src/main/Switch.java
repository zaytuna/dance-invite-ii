package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import util.Vector2D;

public class Switch {
	boolean state = false;
	Vector2D pos;
	double prc = 0;
	String name;

	public Switch(Vector2D p, boolean b, String s) {
		this.state = b;
		this.name = s;
		pos = p;
	}

	public void update() {
		prc += ((state ? 1 : 0) - prc) / 5;
	}

	public void paint(Graphics2D g, ViewPane q, int w) {
		g.setColor(state ? Color.GREEN : Color.RED);
		g.fillArc(q.rnd(pos.x - q.view.x) - w / 2, q.rnd(pos.y - q.view.y) - w
				/ 2, w, w, -90, 180);
		g.setStroke(new BasicStroke(4));
		g.setColor((state ? Color.GREEN : Color.RED).darker().darker());
		g.drawLine(
				q.rnd(pos.x - q.view.x),
				q.rnd(pos.y - q.view.y),
				q.rnd(pos.x - q.view.x)
						+ (int) (w / 2 * 1.3 * Math.cos(Math.PI / 2
								- (2 * prc * Math.PI / 3 + Math.PI / 6))),
				q.rnd(pos.y - q.view.y)
						+ (int) (w / 2 * 1.3 * Math.sin(Math.PI / 2
								- (2 * prc * Math.PI / 3 + Math.PI / 6))));
	}
}
