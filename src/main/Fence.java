package main;

import java.awt.Graphics2D;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import util.Vector2D;

public class Fence {
	public static BufferedImage IMAGE;
	public static TexturePaint PAINT;
	Vector2D[] pts;

	public Fence(Vector2D... v) {
		pts = v;
	}

	public void paint(Graphics2D g, ViewPane v) {
		g.setPaint(PAINT);
		int[] xpts = new int[pts.length], ypts = new int[pts.length];
		for (int i = 0; i < pts.length; i++) {
			xpts[i] = v.rnd(pts[i].x);
			ypts[i] = v.rnd(pts[i].y);
		}

		g.translate(-v.rnd(v.view.x), -v.rnd(v.view.y));

		g.fillPolygon(xpts, ypts, pts.length);
		g.translate(v.rnd(v.view.x), v.rnd(v.view.y));
	}

	public boolean contains(Vector2D p) {
		if (pts.length < 3)
			return false;

		boolean oddNodes = false;
		double x2 = pts[pts.length - 1].x;
		double y2 = pts[pts.length - 1].y;
		double x1, y1;
		for (int i = 0; i < pts.length; x2 = x1, y2 = y1, ++i) {
			x1 = pts[i].x;
			y1 = pts[i].y;
			if (((y1 < p.y) && (y2 >= p.y)) || (y1 >= p.y) && (y2 < p.y)) {
				if ((p.y - y1) / (y2 - y1) * (x2 - x1) < (p.x - x1))
					oddNodes = !oddNodes;
			}
		}
		return oddNodes;
	}
}
