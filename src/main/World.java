package main;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;

import util.Methods;
import util.Vector2D;

//SINGLETON
public class World {
	public static Image BACKGROUND, FOREGROUND;
	public static double DIFF = 1.3;// >1

	public Wall[] WALLS = { new Wall(3, 3, 180, 50), new Wall(3, 20, 180, 20) };
	public static World WORLD = new World();

	public ArrayList<Projectile> shots = new ArrayList<Projectile>();
	public ArrayList<Spawn> spawners = new ArrayList<Spawn>();
	public ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	public ArrayList<Person> people = new ArrayList<Person>();
	public ArrayList<Fence> fences = new ArrayList<Fence>();
	public ArrayList<Checkpoint> checks = new ArrayList<Checkpoint>();
	public ArrayList<Switch> switches = new ArrayList<Switch>();

	public ArrayList<GeneralPath> polygons = new ArrayList<GeneralPath>();

	public MinesweeperGame game = new MinesweeperGame(38, 15, 80);

	private World() {// generate the world for the entire setup
		spawners.add(new Spawn(new Vector2D(41.76, 14.62)));
		spawners.add(new Spawn(new Vector2D(45.5, 14.64)));
		spawners.add(new Spawn(new Vector2D(45.5, 18.21)));
		spawners.add(new Spawn(new Vector2D(67, 46)));

		checks.add(new Checkpoint(new Vector2D(8, 3.5)));
		checks.add(new Checkpoint(new Vector2D(30.4, 40)));
		checks.add(new Checkpoint(new Vector2D(60, 15)));
		checks.add(new Checkpoint(new Vector2D(59, 28.1)));
		checks.add(new Checkpoint(new Vector2D(63, 75)));

		people.add(new Person(new Vector2D(13, 5), "Hey, Rebecca!", Methods
				.randomColor()));
		people.add(new Person(new Vector2D(4.4, 24.4), "Woa, really?", Methods
				.randomColor()));
		people.add(new Person(new Vector2D(15.2, 32.45), "[-.-]", Methods
				.randomColor()));
		people.add(new Person(new Vector2D(13.4, 23), "Weeeee!", Methods
				.randomColor()));
		people.add(new Person(new Vector2D(22.6, 38.4),
				"The guy down there is magic.", Methods.randomColor()));
		people.add(new Person(new Vector2D(23.3, 39.5), "Just ask him!",
				Methods.randomColor()));
		people.add(new Person(new Vector2D(23.5, 50.5),
				"[Space] wall jumping is fun!", Methods.randomColor()));
		people.add(new Person(new Vector2D(23.9, 85.1),
				"For compatiability: An excellent choice...", Methods
						.randomColor()));
		people.add(new Person(
				new Vector2D(28, 85.6),
				"... but to achieve your goals have to get rid of these irrelevent hedgehogs.",
				Methods.randomColor()));

		fences.add(new Fence(new Vector2D(70.2, 43), new Vector2D(70.2, 45),
				new Vector2D(93.4, 45), new Vector2D(93.4, 43)));
		fences.add(new Fence(new Vector2D(56, 35), new Vector2D(56, 37),
				new Vector2D(73, 40), new Vector2D(73, 35)));

		switches.add(new Switch(new Vector2D(68.1, 31), false, "SWITCH1"));
	}

	public Vector2D viewTo(Vector2D p, Vector2D plr) {
		return plr.clone();
	}

	public Vector2D gravity(Vector2D p) {
		if (World.WORLD.polygons.get(2).contains(p.x, p.y))
			return new Vector2D(-0.004, 0);
		return new Vector2D(0, 0);
	}

	public int getMusic(Vector2D p) {
		for (int i = 0; i < World.WORLD.polygons.size(); i++) {
			if (World.WORLD.polygons.get(i).contains(p.x, p.y))
				return i;
		}
		return 4;
	}

	public double scaleTo(Vector2D p) {
		if ((p.clone().minus(new Vector2D(186, 79))).mag2() <= 25) {
			DIFF = 1;
			return Toolkit.getDefaultToolkit().getScreenSize().width / 200;
		}
		DIFF = 1.3;
		return 80;
	}

	public Wall find(String str) {
		for (Wall w : WALLS)
			if (w.name.equalsIgnoreCase(str))
				return w;
		return null;
	}

	public double angleTo(Vector2D p, double angle, double s) {
		if (s < 60)
			return 0;
		if (World.WORLD.polygons.get(0).contains(p.x, p.y)) {
			if (p.y < 20 && p.x < 25)
				return 0;
			if (p.x < 25.67 && p.y > 22 && p.y < 37.3)
				return Math.PI / 3;
			if (p.x > 25.67 && p.y > 0 && p.y < 45.3)
				return -Math.PI / 3;
			return 0;
		}
		if (World.WORLD.polygons.get(1).contains(p.x, p.y)) {
			return 0;
		}
		if (World.WORLD.polygons.get(2).contains(p.x, p.y)) {
			return Math.PI / 2;
		}
		if (World.WORLD.polygons.get(4).contains(p.x, p.y)) {
			return Math.atan2(p.y, p.x);
		}
		/*
		 * else if (p.x < 30 && p.y > 25 && p.y < 50) return -Math.PI / 2; else
		 * if (p.x > 25 && p.x < 45 && p.y > 10) return Math.PI / 4;
		 */
		return 0;
	}

	public Wall[] getWalls() {
		return WALLS;
	}

	public void switchWall(String string, boolean t) {
		Wall w = find(string);
		if (w != null)
			w.operational = t;
	}

	public void addSpikedWallsTo(ArrayList<Wall> temp) {
		Wall w = new Wall(45.0, 55.1, 45.0, 58.2);
		w.width = 5;
		w.spiked = true;
		temp.add(w);

		Wall w2 = new Wall(56.2, 45, 56.2, 47.5);
		w2.width = 5;
		w2.spiked = true;
		temp.add(w2);

		Wall w3 = new Wall(90.5, 46, 90.5, 48.5);
		w3.width = 5;
		w3.spiked = true;
		temp.add(w3);
	}
}
