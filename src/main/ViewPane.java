package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JPanel;

import util.Methods;
import util.MusicPlayer;
import util.Vector2D;

public class ViewPane extends JPanel implements Runnable, KeyListener,
		MouseListener {
	private static final long serialVersionUID = 3313534923518298811L;

	Vector2D view = new Vector2D(0, 0), viewTo; // CENTER OF THE SCREEN
	double scale = 80, rotation = 0;

	Player plr;
	HashSet<Integer> down = new HashSet<Integer>();
	MusicPlayer music = new MusicPlayer(0);
	boolean started = false;

	double rotTo;

	Vector2D start;

	int fps, minFPS;
	long lastTime;

	public int loading = 0, counter = 0;

	double xMax = view.x + getWidth() / scale / 2, xMin = view.x - getWidth()
			/ scale / 2;
	double yMax = view.y + getHeight() / scale / 2, yMin = view.y - getHeight()
			/ scale / 2;
	MinesweeperGame game = World.WORLD.game;
	Checkpoint lastCheck;
	Switch currentSwitch;

	ReentrantLock lock = new ReentrantLock();

	Vector2D mousePos;

	public ViewPane() {
		setDoubleBuffered(false);
		addKeyListener(this);
		addMouseListener(this);

		Toolkit tools = Toolkit.getDefaultToolkit();
		setCursor(tools.createCustomCursor(new BufferedImage(1, 1,
				BufferedImage.TYPE_INT_ARGB), new Point(1, 1), "Blanks"));
		lastCheck = World.WORLD.checks.get(0);
		setPlayer(new Player(lastCheck));
		new Thread(this).start();
	}

	public Vector2D updateMouse() {
		Point p = Methods.mouse();
		Point l = getLocationOnScreen();
		if (l != null) {
			Vector2D v = new Vector2D((p.x - l.x - getWidth() / 2) / scale,
					(p.y - l.y - getHeight() / 2) / scale).rotate(rotation);
			mousePos = v.add(view).clone();
			plr.lookDir = v.minus(plr.pos).angle();
			return v;
		}
		return new Vector2D(0, 0);
	}

	public void go() {
		started = true;
	}

	public void setPlayer(Player p) {
		plr = p;
	}

	public void paintComponent(Graphics go) {
		long t = System.currentTimeMillis();
		fps = (int) (1000f / (t - lastTime));
		if (fps < minFPS)
			minFPS = fps;
		lastTime = t;

		Graphics2D g = (Graphics2D) go;
		g.setFont(MinesweeperGame.FONT);

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());

		if (started) {
			g.translate(getWidth() / 2, getHeight() / 2);
			g.rotate(-rotation);

			g.drawImage(World.BACKGROUND, rnd(-view.x), rnd(-view.y), rnd(200),
					rnd(100), this);

			for (int i = 0; i < World.WORLD.polygons.size(); i++) {
				// World.WORLD.polygons.get(i).transform(
				// AffineTransform.getTranslateInstance(rnd(-view.x),
				// rnd(-view.y)));
				// g.setColor(Methods.randomColor(i, 200));
				// g.fill(World.WORLD.polygons.get(i));
				// World.WORLD.polygons.get(i).transform(
				// AffineTransform.getTranslateInstance(rnd(view.x),
				// rnd(view.y)));
			}

			lock.lock();
			if (scale >= 60
					&& World.WORLD.polygons.get(1).contains(plr.pos.x,
							plr.pos.y)
					&& new Rectangle2D.Double(xMin, yMin, xMax - xMin, yMax
							- yMin).intersects(game.bnds)) {

				// if minesweeper is there, draw it
				double a = game.bnds.width / game.getWidth(), b = game.bnds.height
						/ game.getHeight();

				game.paintSquare(g, -1, -1, rnd(game.bnds.x + game.bnds.width
						/ 2 - view.x - a / 2), rnd(game.bnds.y - b * 1.5
						- view.y), rnd(a), rnd(b));

				for (int i = 0; i < game.getWidth(); i++) {
					for (int j = 0; j < game.getHeight(); j++) {
						game.paintSquare(g, i, j, rnd(game.bnds.x + i * a
								- view.x), rnd(game.bnds.y + j * b - view.y),
								rnd(a) - 7, rnd(b) - 7);
					}
				}
			}

			for (Fence f : World.WORLD.fences)
				f.paint(g, this);
			for (Checkpoint c : World.WORLD.checks)
				c.paint(g, this);
			for (Enemy e : World.WORLD.enemies)
				e.paint(g, rnd(e.pos.x - view.x), rnd(e.pos.y - view.y),
						rnd(0.8));
			for (Switch s : World.WORLD.switches)
				s.paint(g, this, rnd(0.8));
			for (Person p : World.WORLD.people)
				p.paint(g, this);
			for (Spawn s : World.WORLD.spawners)
				s.draw(g, rnd(s.pos.x - view.x), rnd(s.pos.y - view.y),
						rnd(1.5));
			for (Wall w : World.WORLD.getWalls())
				w.draw(g, this);
			for (int i = 0; i < World.WORLD.shots.size(); i++)
				World.WORLD.shots.get(i).paint(g, this);

			plr.paint(g, this);

			g.drawImage(World.FOREGROUND, rnd(-view.x * World.DIFF),
					rnd(-view.y * World.DIFF), rnd(200 * World.DIFF),
					rnd(100 * World.DIFF), this);

			g.rotate(rotation);
			g.translate(-getWidth() / 2, -getHeight() / 2);

			lock.unlock();

			// UI!
			if (plr.weaponNum >= 0) {
				g.setColor(new Color(0, 0, 0, 200));
				g.fillRoundRect(-300, getHeight() - 230, getWidth() / 3 + 300,
						330, 300, 200);

				g.setColor(Color.WHITE);
				g.setFont(new Font("SERIF", Font.BOLD, 80));
				Methods.drawCenteredText(g, "" + plr.level, 62,
						getHeight() - 62, 0, 0);
				g.setColor(Color.CYAN);
				g.setStroke(new BasicStroke(5));
				g.drawArc(5, getHeight() - 120, 115, 115, 0,
						(int) (360 * plr.xp));
				g.setColor(plr.weapon.overheat ? (counter % 30 < 15 ? new Color(
						180, 0, 0, 180) : new Color(0, 0, 0, 0))
						: new Color(180, 0, 180, 180));
				g.fillRoundRect(135, getHeight() - 50,
						(int) ((getWidth() / 3 - 155) * plr.weapon.heat), 15,
						7, 7);
				g.setColor(Color.GREEN);
				g.fillRoundRect(135, getHeight() - 25,
						(int) ((getWidth() / 3 - 155) * plr.hp / plr.max), 15,
						7, 7);

				int dx = getWidth() / 4 / Player.ATTRIBUTES.length;

				g.setColor(Color.GRAY);
				g.setFont(new Font("SERIF", Font.PLAIN, 14));
				for (int i = 0; i < plr.points.length; i++)
					Methods.drawCenteredText(g, Player.ATTRIBUTES[i], 40 + i
							* dx, getHeight() - 160, 0, 0);

				g.setFont(new Font("SERIF", Font.BOLD, 20));
				for (int i = 0; i < plr.points.length; i++)
					Methods.drawCenteredText(g, plr.points[i] + "",
							40 + i * dx, getHeight() - 135, 0, 0);

				g.setColor(Color.WHITE);
				g.setFont(new Font("SERIF", Font.PLAIN, 25));
				if (plr.unspent > 0)
					for (int i = 0; i < plr.points.length; i++) {
						Methods.drawCenteredText(
								g,
								"[" + (i + 1) + "]",
								40 + i * dx,
								getHeight()
										- 210
										+ (int) Math.abs(20 * Math.sin(counter
												/ 20D + i)), 0, 0);
					}

			}
			g.setColor(Color.WHITE);
			g.drawString(fps + ", " + minFPS, 20, 40);

		} else {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(Color.GRAY);
			g.setFont(new Font("SERIF", Font.PLAIN, 180));
			Methods.drawCenteredText(g, "Loading...[" + loading + "]", 0, 0,
					getWidth(), getHeight());
		}
	}

	public int rnd(double d) {
		return (int) (scale * d);
	}

	public void update() {
		if (started) {
			counter++;

			updateMouse();

			if (counter % 100 == 0) {
				int newM = World.WORLD.getMusic(plr.pos);
				if (music.getCurrentIndex() != newM) {
					music.setMusicTo(newM);
				}
				minFPS = fps;

				World.WORLD.switchWall("DOOR2", !game.uncovered());
				World.WORLD
						.switchWall("DOOR1", World.WORLD.spawners.size() > 1);
				World.WORLD.switchWall("DOOR3",
						World.WORLD.switches.get(0).state);

			}

			if (plr.hp <= 0) {
				setPlayer(new Player(lastCheck));
				started = false;
				new Thread(new Runnable() {
					public void run() {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						go();
					}
				}).start();

			}

			xMax = view.x + getWidth() / scale / 2;
			xMin = view.x - getWidth() / scale / 2;
			yMax = view.y + getHeight() / scale / 2;
			yMin = view.y - getHeight() / scale / 2;

			scale += (World.WORLD.scaleTo(plr.pos) - scale) / 10;

			for (int i = 0; i < World.WORLD.shots.size(); i++) {
				Projectile p = World.WORLD.shots.get(i);
				if (p.phase == 2)
					World.WORLD.shots.remove(i--);
				for (Wall wall : World.WORLD.getWalls())
					wall.update(p);
				p.update();
			}

			for (int i = 0; i < World.WORLD.enemies.size(); i++) {
				Enemy e = World.WORLD.enemies.get(i);
				if (e.hp < 0) {
					World.WORLD.enemies.remove(i);
					i--;
					plr.xp += 1d / (2 + plr.level);
					break;
				}

				for (Wall wall : World.WORLD.getWalls())
					wall.update(e);
				e.update(plr);
			}

			for (Checkpoint c : World.WORLD.checks) {
				if ((c.pos.clone().minus(plr.pos)).mag2() < 4) {
					lastCheck.active = false;
					c.active = true;
					lastCheck = c;
				}
			}

			for (Person p : World.WORLD.people)
				p.update(this);
			currentSwitch = null;
			for (Switch s : World.WORLD.switches) {
				if ((s.pos.clone().minus(plr.pos)).mag2() < 2)
					currentSwitch = s;
				s.update();
			}
			for (int i = 0; i < World.WORLD.spawners.size(); i++) {
				World.WORLD.spawners.get(i).update(plr);
				if (World.WORLD.spawners.get(i).hp < 0) {
					World.WORLD.spawners.remove(i);
					i--;
					plr.xp += 3d / (2 + plr.level);
					break;
				}
			}

			Vector2D mines = game.getSquare(plr.pos);

			if (mines.x >= 0 && mines.x < game.getWidth() && mines.y >= 0
					&& mines.y < game.getHeight())
				game.press((int) mines.x, (int) mines.y, true);
			if (Math.abs(mines.x - game.getWidth() / 2) < 0.5 && mines.y > -1.5
					&& mines.y < -0.5) {
				boolean b = game.press(-1, -1, true);
				if (b)
					plr.hpTo -= 10;
			}
			lock.lock();

			plr.update(down, rotation);
			for (Wall wall : World.WORLD.getWalls()) {
				wall.update(plr);
			}
			// plr.update(down, rotation);

			Vector2D v = World.WORLD.viewTo(view, plr.pos);
			view.add((v.add(view.clone().negate())).scale(0.1));
			if (getWidth() / 2 / scale < 100)
				view.x = Math.min(Math.max(getWidth() / 2 / scale, view.x), 200
						- getWidth() / 2 / scale);
			else
				view.x = 100;
			if (getHeight() / 2 / scale < 50)
				view.y = Math.min(Math.max(getHeight() / 2 / scale, view.y),
						100 - getHeight() / 2 / scale);
			else
				view.y = 50;

			double wT = World.WORLD.angleTo(view, plr.walkDir, scale), d = (wT - rotation) / 12;
			if (Math.abs(rotation - wT) > 0.01)
				rotation += Math.signum(d) * Math.min(.1, Math.abs(d));
			else
				rotation = wT;

			lock.unlock();
		}
		// rotation = Math.sin(plr.pos.y+plr.pos.x)/100;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(10);
			} catch (Exception e) {
			}

			update();
			repaint();
		}
	}

	public void keyPressed(KeyEvent evt) {
		down.add(evt.getKeyCode());

		if (evt.getKeyCode() == KeyEvent.VK_Q) {
			plr.weaponNum = (plr.weaponNum - 1) % 2;
			Toolkit tools = Toolkit.getDefaultToolkit();

			if (plr.weaponNum >= 0) {
				setCursor(tools.createCustomCursor(tools
						.getImage("Resources/cursor.png"), new Point(10, 10),
						"X"));
			} else {
				setCursor(tools
						.createCustomCursor(new BufferedImage(1, 1,
								BufferedImage.TYPE_INT_ARGB), new Point(1, 1),
								"Blanks"));
			}
		} else if (evt.getKeyCode() == KeyEvent.VK_M) {
			System.out.println(plr.pos.x + "\t" + plr.pos.y);
		} else {
			for (int i = 0; i < plr.points.length; i++) {
				if (evt.getKeyCode() == KeyEvent.VK_1 + i && plr.unspent > 0) {
					plr.points[i]++;
					plr.unspent--;
				}
			}
		}
	}

	public void keyReleased(KeyEvent evt) {
		down.remove(evt.getKeyCode());

		if (evt.getKeyCode() == KeyEvent.VK_E) {
			if (currentSwitch != null)
				currentSwitch.state = !currentSwitch.state;
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.isMetaDown()) {
			System.out.println(mousePos.x + "\t" + mousePos.y);
		}
		if (scale < 60 || evt.isAltDown() && down.contains(KeyEvent.VK_SHIFT)) {
			plr.pos = mousePos.clone();
			return;
		}
		if (!evt.isMetaDown() && plr.weaponNum >= 0) {
			if (mousePos == null)
				updateMouse();
			plr.weapon.fire(mousePos);
		}
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void keyTyped(KeyEvent arg0) {
	}

}
