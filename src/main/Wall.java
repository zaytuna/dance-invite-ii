package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import util.Methods;
import util.Vector2D;

public class Wall {
	String name = "";
	double x1, x2, y1, y2;
	Color color = Color.ORANGE;
	float width = 5;
	double friction = 1, bounce = 0; // (-.1 -> 1 --> 1.5);
	boolean electric = false;

	boolean operational = true;
	boolean spiked = false;

	public Wall(double a, double b, double c, double d) {
		x1 = a;
		x2 = c;
		y1 = b;
		y2 = d;
	}

	public void draw(Graphics2D g, ViewPane v) {
		g.setStroke(operational ? new BasicStroke(width) : new BasicStroke(
				width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f,
				new float[] { width * 2 }, v.counter / 1f));
		g.setColor(operational ? color : Methods.getColor(color, 100));
		g.drawLine(v.rnd(x1 - v.view.x), v.rnd(y1 - v.view.y),
				v.rnd(x2 - v.view.x), v.rnd(y2 - v.view.y));

		if (spiked) {
			g.setStroke(new BasicStroke(1));
			Vector2D d = new Vector2D(x2 - x1, y2 - y1);
			double m = d.mag();
			d.unitize();
			Vector2D dp = d.clone().perp().scale(width / 20);

			for (float i = 0; i < m; i += 0.2) {
				Vector2D p = d.clone().scale(i).add(new Vector2D(x1, y1));
				g.drawLine(v.rnd(p.x - dp.x - v.view.x),
						v.rnd(p.y - dp.y - v.view.y),
						v.rnd(p.x + dp.x - v.view.x),
						v.rnd(p.y + dp.y - v.view.y));
			}
		}

	}

	public void update(MovingObject plr) {
		if (operational || !(plr instanceof Player)) {
			Vector2D d = new Vector2D(x2 - x1, y2 - y1), pp = plr.pos.clone()
					.minus(new Vector2D(x1, y1));
			Vector2D prd = pp.proj(d);

			pp.minus(prd);

			if (prd.mag() <= d.mag() && prd.dot(d) > 0) {
				Vector2D plat = d.clone();
				d.perp();
				if (pp.dot(d) < 0)
					d.negate();

				if (electric) {
					for (Vector2D v : plr.getVelocities())
						v.add(d.unitize().scale(0.05 / pp.mag2()));
				} else if (pp.mag() < plr.size / 2) {
					if (plr instanceof Player && spiked) {
						((Player) plr).hp = -1;
						return;
					} else if (plr instanceof Enemy && spiked) {
						((Enemy) plr).hp = -1;
						return;
					}
					plr.pos.add(d.unitize().clone()
							.scale(plr.size / 2 - pp.mag()));
					plr.jumpDir = d.clone();

					for (Vector2D v : plr.getVelocities()) {
						Vector2D q = v.proj(plat), r = v.clone().minus(q);
						v.set(q.add(r.scale(-bounce)));
					}
				}
			}
		}
	}

	public static Wall read(String str) {
		String[] pieces = str.split("\t");
		// System.out.println(str);
		Wall w = new Wall(Double.parseDouble(pieces[0]),
				Double.parseDouble(pieces[1]), Double.parseDouble(pieces[2]),
				Double.parseDouble(pieces[3]));

		w.electric = pieces[4].charAt(0) == 't';
		w.friction = Double.parseDouble(pieces[5]);
		w.width = Float.parseFloat(pieces[6]);
		String[] p2 = pieces[7].split(",");
		w.color = new Color(Integer.parseInt(p2[0]), Integer.parseInt(p2[1]),
				Integer.parseInt(p2[2]));

		if (pieces.length > 8 && pieces[8].length() > 0)
			w.bounce = Double.parseDouble(pieces[8]);
		if (pieces.length > 9)
			w.name = pieces[9];

		return w;
	}

	public String toString() {
		String str = x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + electric
				+ "\t" + friction + "\t" + width + "\t" + color.getRed() + ","
				+ color.getGreen() + "," + color.getBlue();

		if (bounce != 0)
			str += "\t" + bounce;
		if (name != null && name.length() > 0)
			str += "\t" + name;
		return str;
	}
}
