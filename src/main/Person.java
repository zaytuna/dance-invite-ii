package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import util.Methods;
import util.Vector2D;

public class Person {
	String text;
	Vector2D pos;
	double walkDir = 0;
	Color color;
	boolean show = false;

	public Person(Vector2D v, String s, Color c) {
		text = s;
		pos = v;
		this.color = c;
	}

	public void update(ViewPane q) {
		Vector2D dx = q.plr.pos.clone().minus(pos);
		walkDir = Math.atan2(dx.y, dx.x) + Math.sin(q.counter / 20f)
				* Math.min(dx.mag() / 3, 2 * Math.PI / 3);
		show = dx.mag2() < 4;
	}

	public void paint(Graphics2D g, ViewPane q) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setStroke(new BasicStroke(4));
		g.translate(q.rnd(pos.x - q.view.x), q.rnd(pos.y - q.view.y));
		g.scale(q.scale / 80, q.scale / 80);

		g.rotate(walkDir);
		g.setColor(color);
		g.fillOval(-12, 4, 44, 32);
		g.fillOval(-12, -45, 44, 32);
		g.setColor(Color.BLACK);
		g.drawOval(-12, 4, 44, 32);
		g.drawOval(-12, -45, 44, 32);
		g.rotate(-walkDir);

		g.setColor(new Color(0, 0, 0, 200));
		g.fillOval(-32, -32, 64, 64);
		g.setColor(Color.GRAY);
		g.setStroke(new BasicStroke(3));
		g.drawOval(-32, -32, 64, 64);

		if (show) {
			g.setFont(new Font("SANS_SERIF", Font.BOLD, 14));

			int w = g.getFontMetrics().stringWidth(text) + 25;
			g.setColor(Methods.getColor(
					Methods.colorMeld(color, Color.BLACK, 0.5), 200));
			g.fillRoundRect(-w / 2, -100, w, 60, 10, 10);
			g.setColor(Methods.colorMeld(color, Color.WHITE, 0.5));
			Methods.drawCenteredText(g, text, -w / 2, -100, w, 60);
		}
		g.scale(80 / q.scale, 80 / q.scale);
		g.translate(-q.rnd(pos.x - q.view.x), -q.rnd(pos.y - q.view.y));
	}
}
