package main;

import java.awt.Graphics2D;
import java.awt.Image;

import util.Vector2D;

public class Checkpoint {
	public static Image CHECK1, CHECK2;
	Vector2D pos;
	boolean active;
	int counter = 0;

	public Checkpoint(Vector2D v) {
		this.pos = v;
		active = false;
	}

	public void paint(Graphics2D g, ViewPane pane) {
		Image img = active ? CHECK1 : CHECK2;
		g.drawImage(img, pane.rnd(pos.x - pane.view.x) - pane.rnd(0.8),
				pane.rnd(pos.y - pane.view.y) - pane.rnd(0.8), pane.rnd(1.6),
				pane.rnd(1.6), pane);
	}

}
