package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import util.Methods;

public class ProgramDriver extends JFrame {

	private static final long serialVersionUID = -8266481434872958383L;

	public static final boolean LOAD_BACKGROUND = true;
	public static final boolean LOAD_FOREGROUND = true;

	public static void main(String args[]) {
		Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();
		int w = 1200, h = 1000;

		ProgramDriver pd = new ProgramDriver();
		pd.setLayout(null);

		ViewPane vp = new ViewPane();

		pd.addKeyListener(vp);
		pd.setSize(SCREEN);
		vp.setSize(w, h);
		vp.setLocation((SCREEN.width - w) / 2, (SCREEN.height - h) / 2);
		pd.getContentPane().setBackground(Color.BLACK);
		pd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pd.setUndecorated(true);
		pd.add(vp);

		pd.setVisible(true);

		try {
			String[] str = Methods.getFileContents(
					new File("Resources/walls.txt")).split("\n");
			ArrayList<Wall> temp = new ArrayList<Wall>();
			for (int i = 0; i < str.length; i++)
				if (str[i].length() > 0 && !str[i].startsWith("*"))
					temp.add(Wall.read(str[i]));

			World.WORLD.addSpikedWallsTo(temp);

			World.WORLD.WALLS = new Wall[temp.size()];
			for (int i = 0; i < temp.size(); i++)
				World.WORLD.WALLS[i] = temp.get(i);

			// READ POLYGONS
			BufferedReader br = new BufferedReader(new FileReader(
					"Resources/polygons.txt"));
			String next = br.readLine();
			GeneralPath current = new GeneralPath();
			while (next != null) {
				if (next.startsWith("**")) {
					current.closePath();
					// current.transform(AffineTransform.getScaleInstance(80,
					// 80));
					World.WORLD.polygons.add(current);
					current = new GeneralPath();
				} else {
					String[] pieces = next.split("\t");
					if (current.getCurrentPoint() == null)
						current.moveTo(Double.parseDouble(pieces[0]),
								Double.parseDouble(pieces[1]));
					else
						current.lineTo(Double.parseDouble(pieces[0]),
								Double.parseDouble(pieces[1]));
				}
				next = br.readLine();
			}
			current.closePath();
			World.WORLD.polygons.add(current);
			br.close();
			// System.out.println(World.WORLD.polygons.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		vp.loading = 1;
		vp.repaint();
		try {
			if (LOAD_BACKGROUND)
				World.BACKGROUND = ImageIO.read(new File(
						"Resources/BACKGROUND.jpg"));
			else
				World.BACKGROUND = new BufferedImage(10, 10,
						BufferedImage.TYPE_INT_ARGB);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.gc();

		vp.loading = 2;
		vp.repaint();
		try {
			MinesweeperGame.BOMB = ImageIO.read(new File("Resources/BOMB.png"));
			MinesweeperGame.RESTART = ImageIO.read(new File(
					"Resources/Restart.png"));
			MinesweeperGame.FONT = new Font("SANS_SERIF", Font.BOLD, 30);
			Enemy.ICON = ImageIO.read(new File("Resources/ENEMY.png"));
			Fence.IMAGE = ImageIO.read(new File("Resources/FENCE.png"));
			Fence.PAINT = new TexturePaint(Fence.IMAGE, new Rectangle2D.Double(
					0, 0, Fence.IMAGE.getWidth(null),
					Fence.IMAGE.getHeight(null)));
			Checkpoint.CHECK1 = ImageIO.read(new File("Resources/CHECK1.png"));
			Checkpoint.CHECK2 = ImageIO.read(new File("Resources/CHECK2.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		vp.loading = 3;
		vp.repaint();
		try {
			if (LOAD_FOREGROUND)
				World.FOREGROUND = ImageIO.read(new File(
						"Resources/FOREGROUND.png"));
			else
				World.FOREGROUND = new BufferedImage(10, 10,
						BufferedImage.TYPE_INT_ARGB);

		} catch (Exception e) {
			e.printStackTrace();
		}
		vp.loading = 4;
		vp.repaint();

		// DONE LOADING!
		vp.go();
	}
}