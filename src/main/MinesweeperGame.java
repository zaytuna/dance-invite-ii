package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Rectangle2D;

import util.Methods;
import util.Vector2D;

public class MinesweeperGame {
	int number;
	public static Image BOMB;
	public static Image RESTART;
	public static Font FONT;

	int[][] clues;
	int[][] covered;
	Color[] colors = { Color.BLUE, new Color(0, 150, 0), new Color(150, 0, 0),
			new Color(150, 0, 150), new Color(100, 100, 0), Color.PINK,
			Color.ORANGE, Color.BLACK };

	int rad = 1;
	Rectangle2D.Double bnds = new Rectangle2D.Double(67.89, 14.13,
			101.0 - 67.89, 26.56 - 15.13);

	boolean show = false;

	int lastX = -1, lastY = -1;

	public MinesweeperGame(int x, int y, int r) {
		number = r;
		setup(x, y, r);
	}

	public void setup(int x, int y, int r) {
		show = false;
		System.out.print("STARTING SETUP");
		clues = new int[x][y];
		covered = new int[x][y];

		int[] nums = new int[x * y];
		for (int i = 0; i < x * y; i++)
			nums[i] = i < r ? -1 : 0;
		Methods.shuffle(nums);

		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++) {
				clues[i][j] = nums[i * y + j];
				covered[i][j] = 1;
			}

		for (int i = 0; i < x; i++)
			for (int j = 0; j < y; j++)
				if (clues[i][j] >= 0)
					for (int k = -rad; k <= rad; k++)
						for (int l = -rad; l <= rad; l++)
							if (i + k >= 0 && i + k < clues.length
									&& j + l >= 0 && j + l < clues[0].length) {
								clues[i][j] += clues[i + k][j + l] < 0 ? 1 : 0;
							}

		System.out.print("..DONE\n");
	}

	public int getWidth() {
		return clues.length;
	}

	public int getHeight() {
		return clues[0].length;
	}

	public void paintSquare(Graphics2D g, int i, int j, int x, int y, int w,
			int h) {
		if (i == -1 && j == -1) {
			g.drawImage(RESTART, x, y, w, h, null);
		} else {

			if (covered[i][j] == 2) {
				g.setColor(new Color(150, 150, 150));
				g.fillRect(x, y, w, h);
				g.setColor(new Color(clues[i][j] > 0 && show ? 0 : 150, 0,
						clues[i][j] > 0 && show ? 150 : 0));
				g.fillRect(x + w / 6, y + h / 3, 2 * w / 3, h / 3);
			} else {
				if (clues[i][j] < 0) {
					g.drawImage(BOMB, x, y, w, h, null);
				} else if (clues[i][j] > 0) {
					g.setColor(colors[Math.abs(clues[i][j] - 1) % colors.length]);
					Methods.drawCenteredText(g, "" + clues[i][j], x, y, w, h);
				}
			}

			if (covered[i][j] == 1) {
				g.setColor(new Color(150, 150, 150, show ? 0 : 255));
				g.fillRect(x, y, w, h);
			}

			g.setColor(Color.DARK_GRAY);
			g.drawRect(x, y, w, h);
		}
	}

	/*
	 * public Point getSquare(Point mouse) { int w = Math.min((width - 5) /
	 * clues.length, (height - 5) / clues[0].length); int x = (mouse.x - (width
	 * - w * clues.length) / 2) / w; int y = (mouse.y - ((height - w *
	 * clues[0].length) / 2)) / w;
	 * 
	 * if (x >= 0 && y >= 0 && x < clues.length && y < clues[x].length) return
	 * new Point(x, y); return null; }
	 */

	public boolean reveal(int x, int y) {
		if (x < 0 || y < 0 || x >= clues.length || y >= clues[x].length
				|| covered[x][y] == 0)
			return true;

		if (covered[x][y] == 2) {
			covered[x][y] = 1;
			return true;
		}

		covered[x][y] = 0;

		if (clues[x][y] == 0) {
			reveal(x + 1, y + 1);
			reveal(x + 1, y);
			reveal(x + 1, y - 1);

			reveal(x, y + 1);
			reveal(x, y - 1);

			reveal(x - 1, y + 1);
			reveal(x - 1, y);
			reveal(x - 1, y - 1);
		}

		return clues[x][y] >= 0;
	}

	public boolean uncovered() {
		for (int i = 0; i < clues.length; i++)
			for (int j = 0; j < clues[i].length; j++)
				if (covered[i][j] == 1)
					return false;

		return true;
	}

	public void revealLargest() {

	}
	
	public Vector2D getSquare(Vector2D v) {
		return new Vector2D((getWidth() * (v.x - bnds.x) / bnds.width), (
				getHeight() * (v.y - bnds.y) / bnds.height));
	}

	public boolean press(int x, int y, boolean b) {
		if (lastX == x && lastY == y)
			return false;
		lastX = x;
		lastY = y;

		if (x == -1 && y == -1 && show)
			setup(clues.length, clues[0].length, number);

		Point p = new Point(x, y);// getSquare(evt.getPoint());

		if (p != null) {
			if (!show) {
				if ((!b) && covered[p.x][p.y] != 2)
					covered[p.x][p.y] = 2;
				else if (!reveal(p.x, p.y)) {
					show = true;
					return true;
				}
			}
		}
		return false;
	}
}
